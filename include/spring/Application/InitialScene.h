#pragma once
#include <spring\Framework\IScene.h>
#include <qgridlayout.h>
#include <qspinbox.h>
#include <qpushbutton.h>
#include <qlabel>
#include <qlineedit.h>
#include <qobject.h>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

namespace Spring
{
	class InitialScene : public IScene
	{
		Q_OBJECT

	public:

		explicit InitialScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;

		~InitialScene();

	private:
		void createGUI();

		QWidget *centralWidget;
		QHBoxLayout *horizontalLayout;
		QSpacerItem *horizontalSpacer;
		QGridLayout *gridLayout;
		QLabel *helloLabel;
		QSpacerItem *verticalSpacer;
		QSpacerItem *verticalSpacer_2;
		QSpacerItem *horizontalSpacer_2;
		QMenuBar *menuBar;
		QStatusBar *statusBar;

		private slots:
	};
}
